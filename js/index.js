const svgElements = document.querySelectorAll('.scribble');

let activeElement = null;
let isDragging = false;
let initialX = 0;
let initialY = 0;
let rotation = 0;

svgElements.forEach((svg) => {
  svg.addEventListener('mousedown', (e) => {
    activeElement = svg;
    isDragging = true;
    initialX = e.clientX - activeElement.getBoundingClientRect().left;
    initialY = e.clientY - activeElement.getBoundingClientRect().top;
    activeElement.style.cursor = 'grabbing';
  });
});

document.addEventListener('mousemove', (e) => {
  if (!isDragging) return;
  const newX = e.clientX - initialX;
  const newY = e.clientY - initialY;
  activeElement.style.left = newX + 'px';
  activeElement.style.top = newY + 'px';
});

document.addEventListener('mouseup', () => {
  isDragging = false;
  if (activeElement) {
    activeElement.style.cursor = 'grab';
  }
});

svgElements.forEach((svg) => {
  svg.addEventListener('contextmenu', (e) => {
    e.preventDefault();
    rotation += 15; // Rotate by 15 degrees
    svg.style.transform = `rotate(${rotation}deg)`;
  });
});





 function darkMode(){
        document.body.classList.add("bg-dark");
        var listPTags= document.getElementsByTagName('p');
          for (var i = 0; i < listPTags.length; i++) {
            listPTags[i].classList.remove("text-secondary");
            listPTags[i].classList.add("text-white");
          }

        var headings = document.querySelectorAll("h1, h2, h3, h4, h5, h6");
        for (var i = 0; i < headings.length; i++) {
          headings[i].classList.add("text-white");
        }

        var sections = document.getElementsByTagName("section");
        for (var i = 0; i < sections.length; i++) {
          if(sections[i].classList.contains("bg-light")){
            sections[i].classList.remove("bg-light");
            sections[i].classList.add("bg-dark");
        }
      }
      }
